from django.shortcuts import render, redirect

# Create your views here.
from .forms import TaskForm
from .models import Task
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            #create_tasks = form.save()
            form.save()
            return redirect("list_projects")
    else:
        form = TaskForm()
    return render(request, "create_task.html", {"form": form})


@login_required
def my_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    return render(request, "my_tasks.html", {"tasks": tasks})
