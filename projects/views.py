from django.shortcuts import render, get_object_or_404, redirect
from .models import Project
from django.contrib.auth.decorators import login_required
from .forms import ProjectForm


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)

    return render(request, "list_projects.html", {"projects": projects})


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    tasks = project.tasks.all()

    return render(
        request, "project_detail.html", {"project": project, "tasks": tasks}
    )


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()
    return render(request, "create_project.html", {"form": form})
